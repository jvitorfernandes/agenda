public class Pessoal extends Contato{

	private String redeSocial;
	private String celular;

	public void setRedeSocial(String umaRedeSocial){
		redeSocial=umaRedeSocial;
	}
	
	public String getRedeSocial(){
		return redeSocial;
	}
	
	public void setCelular(String umCelular){
		celular=umCelular;
	}
	
	public String getCelular(){
		return celular;
	}

}