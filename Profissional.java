public class Profissional extends Contato{

	private String organizacao;
	private String telefoneComercial;
	private String cargo;

	public void setOrganizacao(String umaOrganizacao){
		organizacao=umaOrganizacao;
	}
	
	public String getOrganizacao(){
		return organizacao;
	}
	
	public void setTelefoneComercial(String umTelefoneComercial){
		telefoneComercial=umTelefoneComercial;
	}
	
	public String getTelefoneComercial(){
		return telefoneComercial;
	}
	
	public void setCargo(String umCargo){
		cargo=umCargo;
	}
	
	public String getCargo(){
		return cargo;
	}

}